from entities import CurrentUndoOperation, UndoOperation


# This function creates undo operation for set/unset calls
def update_undo_operations_system(name, existed_value):
    # Create undo operation for the current set/unset operation
    current_undo_operation_key = CurrentUndoOperation.get_current_undo_operation_key()
    next_undo_operation_id = UndoOperation.add_undo_operation(name, existed_value, current_undo_operation_key)

    # Updates the current undo operation to be the undo operation just created
    CurrentUndoOperation.add_or_update_current_undo_operation(next_undo_operation_id)
