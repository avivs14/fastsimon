# This function helps to write text response
def write_text_response(response, text):
    response.headers['Content-Type'] = 'text/plain'
    response.out.write(text)
