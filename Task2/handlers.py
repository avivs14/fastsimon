import webapp2
from entities import NamedValue, CurrentUndoOperation, UndoOperation
from response_helper import write_text_response
from undo_operations_helper import update_undo_operations_system


# Set the value given to the key given, neither variable names nor values will contain spaces.
class SetHandler(webapp2.RequestHandler):
    def get(self):
        name = self.request.get('name')
        value = self.request.get("value")
        if not name:
            write_text_response(self.response, 'Cannot add value when name was not provided')
        else:
            existed_value = NamedValue.add_or_update_value(name, value)
            update_undo_operations_system(name, existed_value)


# Print out the value of the variable name given or 'None' if the variable is not set.
class GetHandler(webapp2.RequestHandler):
    def get(self):
        name = self.request.get('name')
        if not name:
            write_text_response(self.response, 'Cannot look for value when name was not provided')
        else:
            value = NamedValue.query_name(name)
            write_text_response(self.response, value)


# Unset the variable given, making it just like the variable was never set.
class UnsetHandler(webapp2.RequestHandler):
    def get(self):
        name = self.request.get('name')
        if not name:
            write_text_response(self.response, 'Cannot unset the variable when name was not provided')
        else:
            deleted_value = NamedValue.delete_name(name)
            update_undo_operations_system(name, deleted_value)


# Print to the browser the number of variables that are currently set to the provided value
# If no variables equal that value, print 0.
class NumEqualToHandler(webapp2.RequestHandler):
    def get(self):
        value = self.request.get("value")
        if not value:
            write_text_response(self.response, 'Cannot check number of instances when value is not provided')
        else:
            values_count = NamedValue.count_values(value)
            write_text_response(self.response, values_count)


# Undo the most recent SET/UNSET command.
# If more than one consecutive UNDO command is issued,
# the original commands should be undone in the reverse order of their execution.
# Print nothing if successful, or print NO COMMANDS if no commands may be undone.
class UndoHandler(webapp2.RequestHandler):
    def get(self):
        current_undo_key = CurrentUndoOperation.get_current_undo_operation_key()
        if current_undo_key != 0:
            # Gets the necessary info for executing the undo and deleting it after
            undo_operation_to_execute = UndoOperation.get_by_id(current_undo_key)
            next_undo_operation_to_execute = undo_operation_to_execute.NextUndoOperationKey
            UndoOperation.delete_entity(undo_operation_to_execute)

            # Undo
            NamedValue.add_or_update_value(undo_operation_to_execute.Name, undo_operation_to_execute.OldValue)

            # Updates the CurrentUndoOperation to be the next one on the link
            CurrentUndoOperation.add_or_update_current_undo_operation(next_undo_operation_to_execute)
        else:
            write_text_response(self.response, 'NO COMMANDS')


# removes all data from the application.(clean all the Datastore entities)
class EndHandler(webapp2.RequestHandler):
    def get(self):
        NamedValue.delete_all()
        UndoOperation.delete_all()
        CurrentUndoOperation.delete_all()
