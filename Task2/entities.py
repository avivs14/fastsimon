from google.appengine.ext import ndb

BULK_SIZE_FOR_DELETION = 500


# This entity represents a value with a name
class NamedValue(ndb.Model):
    name = ndb.StringProperty(indexed=True)
    value = ndb.StringProperty(indexed=True)

    @classmethod
    def query_name(cls, name):
        result = cls.query(cls.name == name).get()
        if result is None:
            return None
        return result.value

    # This method returns the existed value in case of value update.
    # In case there is an add this method will return None.
    @classmethod
    def add_or_update_value(cls, name, value):
        named_value = cls.query(cls.name == name).get()
        if named_value is None:
            named_value = NamedValue(name=name)
        existed_value = named_value.value
        named_value.value = value
        named_value.put()
        return existed_value

    # This method returns the deleted value in case there is a value.
    # In case there is no such name this method will return None.
    @classmethod
    def delete_name(cls, name):
        named_value = cls.query(cls.name == name).get()
        deleted_value = named_value.value
        if named_value is not None:
            named_value.key.delete()
        return deleted_value

    @classmethod
    def count_values(cls, value):
        return cls.query(cls.value == value).count()

    @classmethod
    def delete_all(cls):
        inner_delete_all(cls)


# This entity represents a single operation that was made at the system.
# New UndoOperation should be added to the store for every set/unset
# Every UndoOperation holds it's next operation key so it's creates kind of linked list of operations
class UndoOperation(ndb.Model):
    Name = ndb.StringProperty(indexed=False)
    OldValue = ndb.StringProperty(indexed=False)
    NextUndoOperationKey = ndb.IntegerProperty(indexed=False)

    @classmethod
    def add_undo_operation(cls, name, old_value, next_undo_operation_key):
        undo_operation = UndoOperation(Name=name, OldValue=old_value, NextUndoOperationKey=next_undo_operation_key)
        undo_operation.put()
        return undo_operation.key.id()

    @classmethod
    def get_by_id(cls, operation_id):
        result = cls.query(cls.key == ndb.Key('UndoOperation', operation_id)).get()
        if result is None:
            return 0
        return result

    @classmethod
    def delete_entity(cls,undo_operation_to_delete):
        undo_operation_to_delete.key.delete()

    @classmethod
    def delete_all(cls):
        inner_delete_all(cls)


# This entity holds the current operation that was made at the system.
# The operation key should be updated after set,unset and undo to always reference the correct operation
class CurrentUndoOperation(ndb.Model):
    OperationKey = ndb.IntegerProperty(indexed=False)

    @classmethod
    def get_current_undo_operation_key(cls):
        result = cls.query().get()
        if result is None:
            return 0
        return result.OperationKey

    @classmethod
    def add_or_update_current_undo_operation(cls, operation_key):
        current_undo_operation = cls.query().get()
        if current_undo_operation is None:
            current_undo_operation = CurrentUndoOperation()
        current_undo_operation.OperationKey = operation_key
        current_undo_operation.put()

    @classmethod
    def delete_all(cls):
        inner_delete_all(cls)


# This function is for deleting all entities of a certain kind
# We need this logic because We don't want to hit the 30 sec timeout of the delete operation
def inner_delete_all(cls):
    list_of_keys = cls.query().fetch(keys_only=True)
    while list_of_keys:
        ndb.delete_multi(list_of_keys[:BULK_SIZE_FOR_DELETION])
        list_of_keys = list_of_keys[BULK_SIZE_FOR_DELETION:]
